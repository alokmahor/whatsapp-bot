# -*- coding: utf-8 -*-
import os, subprocess, time

from yowsup.layers.interface                           import YowInterfaceLayer                 #Reply to the message
from yowsup.layers.interface                           import ProtocolEntityCallback            #Reply to the message
from yowsup.layers.protocol_messages.protocolentities  import TextMessageProtocolEntity         #Body message
from yowsup.layers.protocol_presence.protocolentities  import AvailablePresenceProtocolEntity   #Online
from yowsup.layers.protocol_presence.protocolentities  import UnavailablePresenceProtocolEntity #Offline
from yowsup.layers.protocol_presence.protocolentities  import PresenceProtocolEntity            #Name presence
from yowsup.layers.protocol_chatstate.protocolentities import OutgoingChatstateProtocolEntity   #is writing, writing pause
from yowsup.common.tools                               import Jid                               #is writing, writing pause
import re
from yowsup.layers.protocol_acks.protocolentities      import OutgoingAckProtocolEntity
import json, datetime

ADMIN_HELP = '''Hi %s,\n
All commands are case insensitive

1. To Start new game
admin start team1/team2 at HH:mm
e.g admin start SRH/RCB at 20:00

on start I will start capturing bets

2. To stop game
admin stop team1/team2
e.g admin stop SRH/RCB

this will stop capturing bets

3. To place bet
bet amount team
e.g bet 200 SRH
or bet SRH 200

4. To see caputured data
admin data
'''

START_GAME = '''Caturing has started for %s at %s.
If you ran above command by mistake or entered parameters are wrong then please stop capturing by
admin stop %s

To place bet on %s write
bet 200 %s
or
bet %s 200

To place bet on %s write
bet 200 %s
or
bet %s 200

I will not accept bet after %s'''

name = "Virtual Admin"

class EchoLayer(YowInterfaceLayer):
    GAME = None
    GAME_TIME = None
    TEAM1 = None
    TEAM2 = None
    GAME_MAP = {}
    
    @ProtocolEntityCallback("message")
    def onMessage(self, messageProtocolEntity):
        if messageProtocolEntity.getType() == 'text':
            time.sleep(0.5)
            self.toLower(messageProtocolEntity.ack()) #Set received (double v)
            time.sleep(0.5)
            self.toLower(PresenceProtocolEntity(name = name)) #Set name Presence
            time.sleep(0.5)
            self.toLower(AvailablePresenceProtocolEntity()) #Set online
            time.sleep(0.5)
            self.toLower(messageProtocolEntity.ack(True)) #Set read (double v blue)
            time.sleep(0.5)
            self.toLower(OutgoingChatstateProtocolEntity(OutgoingChatstateProtocolEntity.STATE_TYPING, Jid.normalize(messageProtocolEntity.getFrom(False)) )) #Set is writing
            time.sleep(2)
            self.toLower(OutgoingChatstateProtocolEntity(OutgoingChatstateProtocolEntity.STATE_PAUSED, Jid.normalize(messageProtocolEntity.getFrom(False)) )) #Set no is writing
            time.sleep(1)
            self.onTextMessage(messageProtocolEntity) #Send the answer
            time.sleep(3)
            self.toLower(UnavailablePresenceProtocolEntity()) #Set offline

    @ProtocolEntityCallback("receipt")
    def onReceipt(self, entity):
        print entity.ack()
        #ack = OutgoingAckProtocolEntity(entity.getId(), "receipt", entity.getType(), entity.getFrom())
        #self.toLower(ack)
        self.toLower(entity.ack())

    def onTextMessage(self,messageProtocolEntity):
        namemitt   = messageProtocolEntity.getNotify()
        message    = messageProtocolEntity.getBody().lower()
        recipient  = messageProtocolEntity.getFrom()
        messageTime = datetime.datetime.fromtimestamp(int(messageProtocolEntity.getTimestamp()))
        textmsg    = TextMessageProtocolEntity

        #For a break to use the character \n
        #The sleep you write so #time.sleep(1)
        # admin help
        if re.match('\W*admin\W+help', message, re.IGNORECASE):
            r = re.match('\W*admin\W+help', message, re.IGNORECASE)
            answer = ADMIN_HELP%(namemitt)
            self.toLower(textmsg(answer, to = recipient ))
            print answer

        # admin start SRH/RCB at 20:00
        elif re.match('\W*admin\W+start\W+([\w/]+)\W+at\W+([\d\:]+)', message, re.IGNORECASE):
            r = re.match('\W*admin\W+start\W+([\w/]+)\W+at\W+([\d\:]+)', message, re.IGNORECASE)
            self.GAME = r.group(1).lower()
            self.TEAM1, self.TEAM2 = self.GAME.split('/')
            h, m = r.group(2).split(':')
            today = datetime.datetime.now()
            self.GAME_TIME = datetime.datetime(today.year, today.month, today.day, int(h), int(m))
            self.GAME_MAP[self.GAME] = {'time': self.GAME_TIME}
            self.GAME_MAP[self.GAME][self.TEAM1] = {'total': 0}
            self.GAME_MAP[self.GAME][self.TEAM2] = {'total': 0}
            answer = START_GAME%(self.GAME, self.GAME_TIME, self.GAME, self.TEAM1, self.TEAM1, self.TEAM1, self.TEAM2, self.TEAM2, self.TEAM2, self.GAME_TIME)
            self.toLower(textmsg(answer, to = recipient ))
            print answer
            
        # admin stop srh/rcb
        elif re.match('\W*admin\W+stop\W+([\w/]+)\W*', message, re.IGNORECASE):
            r = re.match('\W*admin\W+stop\W+([\w/]+)\W*', message, re.IGNORECASE)
            g = r.group(1)
            if g in self.GAME_MAP:
                answer = 'Capturing stopped for %s.\nGame Data:\n%s'%(g, json.dumps(self.GAME_MAP, indent=2, default=str))
                self.toLower(textmsg(answer, to = recipient ))
                print answer
                self.GAME_MAP.pop(g)
            else:
                answer = '%s not found in map. Running games are %s'%(g, ','.join(self.GAME_MAP.keys()))
                self.toLower(textmsg(answer, to = recipient ))
                print answer
        
        # admin data
        elif re.match('\W*admin\W*data\W*', message, re.IGNORECASE):
            answer = json.dumps(self.GAME_MAP, indent=2, default=str)
            self.toLower(textmsg(answer, to = recipient ))
            print answer
        
        # bet rcb 200
        elif re.match('\W*bet\W+(\w+)\W+(\w+)\W*', message, re.IGNORECASE):
            r = re.match('\W*bet\W+(\w+)\W+(\w+)\W*', message, re.IGNORECASE)
            try:
                a = int(r.group(1)) if re.match('\d{3,4}', r.group(1)) else int(r.group(2))
                t = r.group(1).lower() if re.match('[^0-9]+', r.group(1)) else r.group(2).lower()
                print 'Hare Krishna', a, t
                game = None
                for key, value in self.GAME_MAP.items():
                    if t in key:
                        game = key
                        t1, t2 = game.split('/')
                        gameTime = self.GAME_MAP[game]['time']
                        tossTime = gameTime - datetime.timedelta(minutes=30)
                        fiveMinTime = gameTime - datetime.timedelta(minutes=5)
                        if messageTime <= tossTime:
                            if a >= 100:
                                if self.GAME_MAP[game][t].get(namemitt):
                                    answer = '%s your bet have already accepted'%(namemitt)
                                    self.toLower(textmsg(answer, to = recipient ))
                                else:
                                    self.GAME_MAP[game][t][namemitt] = {'team': t, 'amount': a}
                                    self.GAME_MAP[game][t]['total'] += a
                                    answer = '%s your bet has accepted: %s on %s\n%s:%s/%s'%(namemitt, a, t, game, self.GAME_MAP[game][t1]['total'], self.GAME_MAP[game][t2]['total'])
                                    self.toLower(textmsg(answer, to = recipient ))
                                    print answer
                            else:
                                answer = '%s amount should be >=100'%(namemitt)
                                self.toLower(textmsg(answer, to = recipient ))
                        elif tossTime < messageTime <= fiveMinTime:
                            if a >= 200:
                                if self.GAME_MAP[game][t].get(namemitt):
                                    answer = '%s your bet have already accepted'%(namemitt)
                                    self.toLower(textmsg(answer, to = recipient ))
                                else:
                                    self.GAME_MAP[game][t][namemitt] = {'team': t, 'amount': a}
                                    self.GAME_MAP[game][t]['total'] += a
                                    answer = '%s your bet has accepted: %s on %s\n%s:%s/%s'%(namemitt, a, t, game, self.GAME_MAP[game][t1]['total'], self.GAME_MAP[game][t2]['total'])
                                    self.toLower(textmsg(answer, to = recipient ))
                                    print answer
                            else:
                                answer = '%s amount should be >=200 now'%(namemitt)
                                self.toLower(textmsg(answer, to = recipient ))
                        elif fiveMinTime < messageTime <= gameTime:
                            if a >= 500:
                                if self.GAME_MAP[game][t].get(namemitt):
                                    answer = '%s your bet have already accepted'%(namemitt)
                                    self.toLower(textmsg(answer, to = recipient ))
                                else:
                                    self.GAME_MAP[game][t][namemitt] = {'team': t, 'amount': a}
                                    self.GAME_MAP[game][t]['total'] += a
                                    answer = '%s your bet has accepted: %s on %s\n%s:%s/%s'%(namemitt, a, t, game, self.GAME_MAP[game][t1]['total'], self.GAME_MAP[game][t2]['total'])
                                    self.toLower(textmsg(answer, to = recipient ))
                                    print answer
                            else:
                                answer = '%s amount should be >=500 now'%(namemitt)
                                self.toLower(textmsg(answer, to = recipient ))
                        elif messageTime > gameTime:
                            answer = '%s your bet not accepted'%(namemitt)
                            self.toLower(textmsg(answer, to = recipient ))
                        break
            except IndexError as e:
                print message, e
                answer = 'bad command: %s'%(message)
                self.toLower(textmsg(answer, to = recipient ))
                
        else:
            pass
